`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 15.03.2021 00:42:35
// Design Name:
// Module Name: window
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module my_window #(
    parameter SIZE = 320
    )(
    input clk,
    input rst,
    input strobe_in,
    input enable,
    input [31:0] in,
    output [31:0] out
    );

    reg [$clog2(SIZE+1)-1:0] cnt;
    wire do_op = (cnt != (SIZE));
    assign out = (do_op ? in : 0);

    always @(posedge clk) begin
        if (rst) begin
            cnt <= SIZE;
        end else if (enable) begin
            cnt <= 0;
        end else if (strobe_in & do_op) begin
            cnt <= cnt + 1;
        end
    end
endmodule
