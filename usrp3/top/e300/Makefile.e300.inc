#
# Copyright 2015 Ettus Research LLC
#

##################################################
# Project Setup
##################################################
TOP_MODULE = e300
# NAME = <Input arg>
# PART_ID = <Input arg>

##################################################
# Include other makefiles
##################################################

include ../../tools/make/viv_preamble.mak

include ip/Makefile.inc
include coregen_dsp/Makefile.srcs
include ../../lib/control/Makefile.srcs
include ../../lib/fifo/Makefile.srcs
include ../../lib/zynq_fifo/Makefile.srcs
include ../../lib/control/Makefile.srcs
include ../../lib/timing/Makefile.srcs
include ../../lib/packet_proc/Makefile.srcs
include ../../lib/vita/Makefile.srcs
include ../../lib/dsp/Makefile.srcs
include ../../lib/radio/Makefile.srcs
include ../../lib/io_cap_gen/Makefile.srcs

##################################################
# Sources
##################################################
TOP_SRCS = e3xx_ps.v \
           ppsloop.v \
           spi_slave.v \
           axi_pmu.v \
           axi_dummy.v

ifeq (E310,$(findstring E310, $(EXTRA_DEFS)))
TOP_SRCS += e310.v
TOP_SRCS += e310_core.v
TOP_SRCS += e310.xdc
TOP_SRCS += e310_timing.xdc
else
TOP_SRCS += e3xx_idle.v
TOP_SRCS += e3xx_idle.xdc
endif

# Vivado seems somewhat unhappy when adding above
TOP_SRCS += e3xx.xdc \
            e3xx_timing.xdc


ifeq (,$(findstring E3XX_SG3, $(EXTRA_DEFS)))
PS7_SRCS = $(IP_PS7_1_SRCS)
else
PS7_SRCS = $(IP_PS7_3_SRCS)
endif

SOURCES = $(abspath $(TOP_SRCS)) \
          $(ZYNQ_FIFO_SRCS) \
          $(FIFO_SRCS) \
          $(VITA_SRCS) \
          $(TIMING_SRCS) \
          $(DSP_SRCS) \
          $(PACKET_PROC_SRCS) \
          $(CONTROL_LIB_SRCS) \
          $(RADIO_SRCS) \
          $(CAT_CAP_GEN_SRCS) \
          $(COREGEN_DSP_SRCS) \
          $(IP_XCI_SRCS) \
          $(PS7_SRCS)

##################################################
# Git Hash
##################################################
SHORT_HASH=$(addprefix GIT_HASH=,$(shell ../../tools/scripts/git-hash.sh))

##################################################
# Dependency Targets
##################################################
.SECONDEXPANSION:

export VIV_SOURCE_FILES=$(SOURCES)
export VIV_VERILOG_DEFS=$(EXTRA_DEFS) $(CUSTOM_DEFS) $(SHORT_HASH)

bin: prereqs $$(SOURCES) ip
	$(call BUILD_VIVADO_DESIGN,$(abspath ./build_e300.tcl),$(TOP_MODULE),$(PART_ID))

.PHONY: bin
